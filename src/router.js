import Vue from "vue";
import Router from "vue-router";
import Home from "./components/Home.vue";
// "axios": "^0.19.0",
Vue.use(Router);

export default new Router({
    mode: "hash",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },
        {
            path: "about",
            name: "about",
            component: () =>
                import("./components/About.vue")
        },
    ]
});